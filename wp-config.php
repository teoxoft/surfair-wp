<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'surfair');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?Q+#@|*=Rq`qVAPzCkQMiAMDh-?#ZQ)J.<`muv+:5`46obc*p0s_T; ?E) )q8Z{');
define('SECURE_AUTH_KEY',  '`v<qnf]L#fb#Wx2N_JVnU%G6D6E.nDLerQ,*x726n`d`yRr]ET+_?`JoI|d](*;(');
define('LOGGED_IN_KEY',    'y7&WP[]7Ra-^` s{L-D1FS|):g+fp[FR,uSlj1a>#d+th|z=a/=}ZS:!9%-#{JJW');
define('NONCE_KEY',        '4G.U(`(EG/<-$Vi$<Ms%<hu}SAQdX!|G_u77./#6>JoL+lpS%&RYzQF6w7upb]{`');
define('AUTH_SALT',        'Qb#1l1$BqQ6.WoRWKuZy?dw&b+EK5I`DpK:J#tXuR]BK?<jmq]*[jGHtd.WB;-H!');
define('SECURE_AUTH_SALT', '/,-Y wVeA~+K*5uAx <B6{jqIrex5M+mnt/it/4<c3td[Kor|GgsfJp?r/Z&Yhpj');
define('LOGGED_IN_SALT',   ';L&!=Zad6/I,Cu9dwL;J~mSG|*D(r7b~`;>Z_-LTM{Ap+7K22HcFZfKnOGVV5:=c');
define('NONCE_SALT',       'aCb!DrffFu+`08-ZO~PRgdu$^[t{A=>t<mj+I B$_X/-s*|=5Q7@7jdWj5x<.MJY');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */


define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
