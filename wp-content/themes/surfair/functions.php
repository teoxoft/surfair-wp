<?php

add_action( 'after_setup_theme', 'surfair_setup' );

if ( ! function_exists( 'surfair_setup' ) ):
function surfair_setup() {

	/* Make Twenty Eleven available for translation.
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Twenty Eleven, use a find and replace
	 * to change 'surfair' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'surfair', get_template_directory() . '/languages' );

	$locale = get_locale();
	$locale_file = get_template_directory() . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

	// This theme styles the visual editor with editor-style.css to match the theme style.
//	add_editor_style( array( 'css/editor-style.css') );


	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );

	// Add support for a variety of post formats
	add_theme_support( 'post-formats', array(
//		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
	) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Primary Menu', 'surfair' ) );
	register_nav_menu( 'footer', __( 'Footer Menu', 'surfair' ) );

}
endif;


/**
 * Sidebars
 */
function surfair_widgets_init() {
	register_sidebar(
		array(
			'name' => __( 'Index Sidear', 'surfair' ),
			'id' => 'index',
			'description' => '1st section on Index page',
			'before_widget' => '<div id="widget-%1$s" class="widget overflow %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<div class="widget-title">',
			'after_title' => '</div>'
		)
	);
	register_sidebar(
		array(
			'name' => __( 'Page Before content', 'surfair' ),
			'id' => 'page-before',
			'description' => 'Section before the content in page',
			'before_widget' => '<div id="widget-%1$s" class="widget overflow %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<div class="widget-title">',
			'after_title' => '</div>'
		)
	);
	register_sidebar(
		array(
			'name' => __( 'Page After content', 'surfair' ),
			'id' => 'page-after',
			'description' => 'Section after the content in page',
			'before_widget' => '<div id="widget-%1$s" class="widget overflow %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<div class="widget-title">',
			'after_title' => '</div>'
		)
	);
}
add_action( 'widgets_init', 'surfair_widgets_init' );





/**
 * Functions
 */

if ( ! function_exists( 'pagination' ) ) :
function pagination() {
	global $wp_query, $wp_rewrite;

	// Don't print empty markup if there's only one page.
	if ( $wp_query->max_num_pages < 2 ) {
		return;
	}

	$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$query_args   = array();
	$url_parts    = explode( '?', $pagenum_link );

	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}

	$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

	$format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

	// Set up paginated links.
	$links = paginate_links( array(
		'base'     => $pagenum_link,
		'format'   => $format,
		'total'    => $wp_query->max_num_pages,
		'current'  => $paged,
		'mid_size' => 1,
		'add_args' => array_map( 'urlencode', $query_args ),
		'prev_text' => __( '&larr; Previous', 'twentyfourteen' ),
		'next_text' => __( 'Next &rarr;', 'twentyfourteen' ),
	) );

	if ( $links ) :

	?>
	<nav class="navigation paging-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'twentyfourteen' ); ?></h1>
		<div class="pagination loop-pagination">
			<?php echo $links; ?>
		</div><!-- .pagination -->
	</nav><!-- .navigation -->
	<?php
	endif;
}
endif;


if ( ! function_exists( 'posted_on' ) ) :
function posted_on() {
	printf( '<a href="%1$s" rel="bookmark"><time class="entry-date" datetime="%2$s">%3$s</time></a>',
		esc_url( get_permalink() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);
}
endif;


if ( ! function_exists( 'comment_item' ) ) :
function comment_item( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	global $post;

	echo '<div id="comment-'.$comment->comment_ID.'" class="comment">';
		echo '<header class="comment-meta vcard">';
				printf( '<span class="comment-author"><cite>%1$s</cite></span>',
					get_comment_author_link()
				);
				printf( '<span class="comment-date"><time datetime="%1$s">%2$s</time></span>',
					get_comment_time( 'c' ),
					sprintf('%1$s at %2$s', get_comment_date(), get_comment_time() )
				);
		echo '</header>';
		echo '<div class="comment-content">'.comment_text().'</div>';
}
endif;


if ( ! function_exists( 'post_is_in_descendant_category' ) ) :
function post_is_in_descendant_category( $cats, $_post = null ) {
	foreach ( (array) $cats as $cat ) {
		// get_term_children() accepts integer ID only
		$descendants = get_term_children( (int) $cat, 'category');
		if ( $descendants && in_category( $descendants, $_post ) )
			return true;
	}
	return false;
}
endif;

/**
 * Includes
 */

require_once get_template_directory() . '/inc/shortcodes.php';
