<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<!-- template content.php -->

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="clearfix">
	<?php
		global $blog_category;

		if (is_single()) {
			the_title('<h1 class="entry-title">', '</h1>');
		} else {
			the_title('<h3 class="entry-title"><a href="'.esc_url(get_permalink()).'">', '</a></h3>');
		}

		echo '<div class="entry-meta">';
			echo '<div class="entry-month">'.esc_html(get_the_date("M.")).'</div>';
			echo '<div class="entry-day">'.esc_html(get_the_date("j")).'</div>';
			foreach((get_the_category()) as $pcategory) { 
				if ($pcategory->cat_ID != $blog_category->cat_ID)
					echo '<span class="entry-category">/ <a href="'.get_category_link($pcategory->cat_ID).'">'.$pcategory->name.'</a></span>';
			} 
		echo '</div>';

		if (is_single()) {
			the_content('<div class="entry-content content">',  '</div>');

			if (comments_open()) comments_template();
		} else {
			if ( has_post_thumbnail() ) echo '<a href="'.esc_url(get_permalink()).'" class="entry-thumbnail"><img src="'.wp_get_attachment_url( get_post_thumbnail_id() ).'" /></a>';

			echo '<div class="entry-excerpt">';
				the_excerpt();
			echo '</div>';
		}
	?>
	</div>

	<?php //edit_post_link('Редактировать', '<span class="edit-link">', '</span>'); ?>
</article>
