<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

	<!-- template page.php -->

	<div class="overflow">
		<?php dynamic_sidebar('page-before'); ?>
	</div>

	<?php while ( have_posts() ) : the_post(); ?>
		<div class="page-wrapper wrapper clearfix">
			<h1><?php the_title(); ?></h1>
			<div class="content">
				<?php the_content(); ?>
			</div>
		</div>
	<?php endwhile;?>

	<div class="overflow">
		<?php dynamic_sidebar('page-after'); ?>
	</div>

<?php get_footer(); ?>