<!DOCTYPE html>

<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimal-ui ,user-scalable=no">
	<meta name="msvalidate.01" content="9FDF0CE2A49B62849732BFAC3297B7B6" />


	<title><?php
		global $page, $paged;
		wp_title( '|', true, 'right' );
		bloginfo( 'name' );
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Page %s', 'surfair' ), max( $paged, $page ) );
	?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11" />

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic,300italic,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/wp-content/themes/surfair/surffonts.css">
	<link rel="stylesheet" href="/wp-content/themes/surfair/surficonfont.css">
	<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/surfair/framework.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/surfair/style.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/surfair/jquery-ui-core.css" />

	<script src="/wp-content/themes/surfair/js/functions.js" type="text/javascript"></script>
	<script src="/wp-content/themes/surfair/js/jquery.min.js" type="text/javascript"></script>
	<script src="/wp-content/themes/surfair/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="/wp-content/themes/surfair/js/jquery.widgets.js" type="text/javascript"></script>
	<script src="/wp-content/themes/surfair/js/jquery.blockUI.js" type="text/javascript"></script>
	<script src="/wp-content/themes/surfair/js/jquery.waypoints.min.js" type="text/javascript"></script>

	<script src="/wp-content/themes/surfair/js/jquery.jplayer.min.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/surfair/js/blue.monday/jplayer.blue.monday.css" />

	<?php wp_head(); ?>


	<script type='text/javascript'>
		/* <![CDATA[ */
		$(function(){

			$('h2').waypoint({
				element: $(this),
				handler: function() {
					$(this.element).addClass('on');
				},
				offset: 'bottom-in-view'
			});

			// slide in animation how-it-works





			//
			// tabs
			//

			$('.tabs').each(function() {
				var ul = $('<ul>').prependTo( $(this) );
				$(this).find('.infoblock').each(function(i, infoblock) {
					$('<li>').append(
						$('<a>', {'href':'#infoblock'+i}).append( $(infoblock).find('.infoblock-title') )
					).appendTo(ul);

					$(infoblock).attr('id','infoblock'+i);
				});
				$(this).addClass('clearfix').tabs();

				$(this).find('.infoblock-container').addClass('overflow');
			});


			$('#mobileNav').hide();

            $('#toggleMenu').on( "click", function() {
            	$('#mobileNav').slideToggle();
            });

            $('#login-drop').hide();

			$('#login-btn').on( "mouseenter", function() {
				$('#login-drop').show();
			});

			 $('#login-btn').on( "mouseleave", function() {
				$('#login-drop').hide();
			});

			//
			// map
			//

			var mapContainer = $('.map');
			if ( mapContainer.size() > 0 ) {

				var ul = $('<ul>', {'class':'map-links'}).prependTo( mapContainer );
				mapContainer.find('.infoblock').each(function(i, infoblock) {
					var coords = $(this).attr('data-coords').split('-');
					$('<li>')
						.css({
							'top': coords[0]+'px',
							'left': coords[1]+'px'
						})
						.append(
							$('<a>', {'href':'#mapItem'+i}).append( $(infoblock).find('.infoblock-title').text() )
						).appendTo(ul);



					$(infoblock).attr('id','mapItem'+i);

					if ($(infoblock).find('.map-brief').size() > 0) {
						$(infoblock).find('.map-brief .more').click(function(){
							$(infoblock).find('.map-brief').hide();
							$(infoblock).find('.infoblock-body').show();
						});
					}
				});
				mapContainer.tabs({
					'create' : function(e, ui) {
						resetBrief(ui.panel);
					},
					'beforeActivate' : function(e, ui) {
						resetBrief(ui.newPanel);
					}
				});
			}
			function resetBrief(panel) {
				if ($(panel).find('.map-brief').size() > 0) {
					$(panel).find('.map-brief').show();
					$(panel).find('.infoblock-body').hide();
				}
			}


			//
			// video
			//

			// jplayer GUI

			var jplayerGUI = '<div id="jp_container" class="jp-video" role="application" aria-label="media player">';
					jplayerGUI += '<div class="jp-type-single">';
						jplayerGUI += '<div id="player" class="jp-jplayer"></div>';
						jplayerGUI += '<div class="jp-gui">';
							jplayerGUI += '<div class="jp-interface">';
								jplayerGUI += '<div class="jp-progress">';
									jplayerGUI += '<div class="jp-seek-bar">';
										jplayerGUI += '<div class="jp-play-bar"></div>';
									jplayerGUI += '</div>';
								jplayerGUI += '</div>';
								jplayerGUI += '<div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>';
								jplayerGUI += '<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>';
								jplayerGUI += '<div class="jp-controls-holder">';
									jplayerGUI += '<div class="jp-controls">';
										jplayerGUI += '<button class="jp-play" role="button" tabindex="0">play</button>';
										jplayerGUI += '<button class="jp-stop" role="button" tabindex="0">stop</button>';
									jplayerGUI += '</div>';
									jplayerGUI += '<div class="jp-volume-controls">';
										jplayerGUI += '<button class="jp-mute" role="button" tabindex="0">mute</button>';
										jplayerGUI += '<button class="jp-volume-max" role="button" tabindex="0">max volume</button>';
										jplayerGUI += '<div class="jp-volume-bar">';
											jplayerGUI += '<div class="jp-volume-bar-value"></div>';
										jplayerGUI += '</div>';
									jplayerGUI += '</div>';
									jplayerGUI += '<div class="jp-toggles">';
										jplayerGUI += '<button class="jp-repeat" role="button" tabindex="0">repeat</button>';
										jplayerGUI += '<button class="jp-full-screen" role="button" tabindex="0">full screen</button>';
									jplayerGUI += '</div>';
								jplayerGUI += '</div>';
							jplayerGUI += '</div>';
						jplayerGUI += '</div>';
					jplayerGUI += '</div>';
				jplayerGUI += '</div>';
			$('.video').prepend(jplayerGUI);
			$('#jp_container').hide();

			// init jPlayer

			$('#player').jPlayer( {
				swfPath: ".",
				supplied: "m4v",
				globalVolume: true,
				useStateClassSkin: true,
				size: {
					width: '100%',
					height: '640px',
					cssClass: ''
				},
				cssSelectorAncestor: '#jp_container',
				autohide: {
					restored: true
				}
			});


			// toggles

			$('.video').find('.infoblock a').click(function() {
				var infoblock = $(this).closest('.infoblock');

				$('#jp_container').show();
				$('#player').jPlayer("setMedia", {
					title: infoblock.find('infoblock-title').text(),
					m4v: $(this).attr('href')
				});
				$('#player').jPlayer("play");

				$('.video .infoblock').removeClass('active');
				infoblock.addClass('active');

				return false;
			});

			// rotate press

			function Press() {
                var press= $('#press-rotate h3'),
                    now = press.filter(':visible'),
                    next = now.next().length ? now.next() : press.first(),
                    speed = 1000;

                now.fadeOut(speed);
                next.fadeIn(2000);
            }

            $(function () {
                setInterval(Press, 4400);
            });


			// poster

			var defaultVideo = $($('.video').find('.infoblock')[0]);
				defaultVideo.addClass('active');

			var poster = $('<div>', {'class':'video-poster'})
				.append( $('<a>', {'class':'btn btn-primary'}).text(defaultVideo.find('.infoblock-title').text()) )
				.prependTo('.video');

			poster.click(function(){
				$(this).hide();

				$('#jp_container').show();
				$('#player').jPlayer("setMedia", {
					title: defaultVideo.find('infoblock-title').text(),
					m4v: defaultVideo.find('a').attr('href')
				});
				$('#player').jPlayer("play");
			});


		});
		/* ]]> */
	</script>
</head>




<body <?php body_class(); ?>>

	<div class="header clearfix">
		<div class="logo">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<?php bloginfo( 'name' ); ?>
			</a>
		</div>

		<div class="primary-nav">
			<?php wp_nav_menu( array('theme_location'=>'primary') ); ?>
		</div>

		<div class="login" id="login-btn">
			<a href="/">Login</a>
		<ul class="login-drop" id="login-drop">
			<li><a href="https://www.surfair.com/surfair-login.html">Members</a></li>
            <li><a href="https://www.surfair.com/air.do/t/html/corpmember">Business Administrators</a></li>
            <li><a href="https://www.surfair.com/air.do?go_uri=%2Fair.do%2Ff%2Fhtml%2Ffamimember&go_err=1020">Friends & Family Admins</a></li>
		</ul>
		</div>
		<div class="toggleMenu" id="toggleMenu">
        		<img src="http://surfair.mqtdesign.ru/wp-content/uploads/2014/12/ham.png" width="30px" height="24px" />
        		</div>

        	<ul id="mobileNav">
        		<li><a href="/how-it-works"><span class="icon-svg12 icon-surf-md"></span><span class="nav-link">How It Works</span></a></li>
        		<li><a href="/destinations"><span class="icon-svg14 icon-surf-md"></span><span class="nav-link">Destinations</span></a></li>
        		<li><a href="/travel-info"><span class="icon-svg11 icon-surf-md"></span><span class="nav-link">Travel Info</span></a></li>
        		<li><a href="https://www.surfair.com/surfair-login.html"><span class="icon-svg10 icon-surf-md"></span><span class="nav-link">Login</span></a></li>
        	</ul>

		<div class="join">
			<a href="/waitlist.html" class="btn btn-primary">Join Us</a>
		</div>
	</div>