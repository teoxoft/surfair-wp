
<div class="footer">
	<div class="footer-wrap clearfix">
		<div class="logo logo-desk">
        			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
        				<?php bloginfo( 'name' ); ?>
        			</a>
        		</div>
        			<div class="logo logo-mobile">
                        			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                        				<?php bloginfo( 'name' ); ?>
                        			</a>
                        		</div>

		<div class="footer-nav">
			<?php wp_nav_menu( array('theme_location'=>'footer') ); ?>
		</div>

		<div class="social">
			<a href="http://www.facebook.com/isurftheskies"><span class="icon-fb social-icon"></span></a>
			<a href="http://www.twitter.com/isurftheskies"><span class="icon-tw social-icon"></span></a>
			<a href="http://www.youtube.com/user/isurftheskies"><span class="icon-yt social-icon"></span></a>
			<a href="http://www.instagram.com/isurftheskies"><span class="icon-ig social-icon"></span></a>
			<a href="http://www.linkedin.com/company/surf-air"><span class="icon-in social-icon"></span></a>
	</div>
	<div class="copyright">&copy; 2014 Surf Airlines, Inc</div>
</div>

        <!-- Justin's tracking script starts here! -->
        <script type="text/javascript">
            piAId = '45522';
            piCId = '7810';
            (function () {
                function async_load() {
                    var s = document.createElement('script');
                    s.type = 'text/javascript';
                    s.src = ('https:' === document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
                    var c = document.getElementsByTagName('script')[0];
                    c.parentNode.insertBefore(s, c);
                }
                if (window.attachEvent) {
                    window.attachEvent('onload', async_load);
                }
                else {
                    window.addEventListener('load', async_load, false);
                }
            })();
        </script>
        <!-- Justin's tracking script ends here! -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-30467941-1', 'auto');
  ga('send', 'pageview');

    var ref = document.referrer || "";
    var externalReferrer = (ref.indexOf("surfair.com") < 0) ? ref : "";

    if(externalReferrer){
       setCookie("referrer", externalReferrer);
    }

    function setCookie(key, value) {
        var expires = new Date();
        expires.setTime(expires.getTime() + (30 * 24 * 60 * 60 * 1000));
        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
    }

</script>

    if(externalReferrer){
       setCookie("referrer", externalReferrer);
    }

    function setCookie(key, value) {
        var expires = new Date();
        expires.setTime(expires.getTime() + (30 * 24 * 60 * 60 * 1000));
        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
    }

</script>

<?php wp_footer(); ?>



</body>
</html>