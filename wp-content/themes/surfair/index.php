<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<!-- template index.php -->

	<div class="blog">
	<div class="overflow">
		<?php dynamic_sidebar('page-before'); ?>
	</div>

	<?php
		// blog category
		$blog_category = get_category_by_slug('blog');

		// current items
		$term = get_queried_object();

		$current_category = null;
		if ( is_category() ) $current_category = $term;

		$current_post = null;
		if ( is_single() ) $current_post = $term;

		// check blog
		if ($blog_category && (cat_is_ancestor_of($blog_category->cat_ID, $current_category->cat_ID) || $blog_category->cat_ID == $current_category->cat_ID || post_is_in_descendant_category($blog_category->cat_ID, $current_post->ID) || in_category($blog_category->cat_ID, $current_post->ID)) ) {
			// blog layout
			echo '
			<div class="top-wrap">
				<div class="wrapper clearfix">
					<div class="follow-text">FOLLOW US:</div>
						<div class="blog-head">
							<ul class="blog-nav">
								<li><a href="#">Surf Air</a></li>
								<li><a href="#">Facebook</a></li>
								<li><a href="#">Twitter</a></li>
								<li><a href="#">YouTube</a></li>
								<li><a href="#">Instagram</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>';
			echo '<div class="page-wrapper wrapper clearfix">';

			// link to blog index
			if ($blog_category->cat_ID != $current_category->cat_ID) {
				echo '<div class="blog-homelink"><a href="/blog" class="btn btn-primary">'.$blog_category->name.'</a></div>';
			}

			// children categories
			$blog_subcategories = get_categories('child_of='.$blog_category->cat_ID);
			if (count($blog_subcategories) > 0) {
				echo '<div class="blog-subcategories overflow">';
					foreach($blog_subcategories as $subcategory) {
						echo '<a href="'.get_category_link($subcategory->cat_ID).'" '.($subcategory->cat_ID == $current_category->cat_ID ? 'class="active"' : '').'>'.$subcategory->name.'</a>';
					}
				echo '</div>';
			}

		} else {
			// simple category layout
			echo '<div class="page-wrapper wrapper clearfix">';
		}

		echo '<h1>'.$current_category->name.'</h1>';
	?>

		<div class="posts">
		<div class="blog-center">
		<?php if ( have_posts() ) : ?>
			<?php 
				while ( have_posts() ) : the_post();
					get_template_part( 'content', get_post_format() );
				endwhile;
				pagination();
			?>
		<?php else : ?>
			<div class="no-data">No entries</div>
		<?php endif; ?>
		</div>
		</div>
	</div>
	</div>

<?php get_footer(); ?>