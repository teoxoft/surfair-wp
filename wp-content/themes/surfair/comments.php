<?php
/**
 * The template for displaying Comments
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to twentytwelve_comment() which is
 * located in the functions.php file.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) return;

echo '<div class="comments">';
echo '<h2>Comments</h2>';
	if (have_comments()) {
		echo '<div class="comment-list">';
			wp_list_comments(
				array(
					'callback' => 'comment_item',
					'style' => 'div',
					'max_depth' => 0,
					'per_page' => 0
				)
			);
		echo '</div>';
	}
	comment_form();
echo '</div>';
?>

<script type='text/javascript'>
	/* <![CDATA[ */
	$(function(){
		$('form.comment-form').on('submit', function(){
			var error = new Array();
			$(this).find('[aria-required]').each(function(){
				if ($(this).val() == '') error.push('"'+$(this).closest('.form-group').find('label').text().replace('*', '')+'"');
			});

			if (error.length > 0) {
				alert('Please fill '+error.join(', ')+' field'+(error.length > 1 ? 's' : '')+'.');
				return false;
			}
		});
	});
	/* ]]> */
</script>
