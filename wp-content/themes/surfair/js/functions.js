
//
// global functions
//

function strReplace(subject, search, replace) {
	return subject.split(search).join(replace);
}

function strPos(haystack, needle, offset) {
	var i = haystack.indexOf(needle, offset);
	return i >= 0 ? i : false;
}

function nl2br(str) {
	return (str + '').replace(/([^>]?)\n/g, '$1'+ '<br/>');
}

function morph(value, word0, word1, word2, separator) {
	if ( (/1\d$/).test(value) ) {
		return value+separator+word2;
	} else if ( (/1$/).test(value) ) {
		return value+separator+word0;
	} else if ( (/(2|3|4)$/).test(value) ) {
		return value+separator+word1;
	} else {
		return value+separator+word2;
	}
}


// cookie

function setCookie(name, value, props) {
	props = props || {};
	var exp = props.expires;

	if (typeof exp == "number" && exp) {
		var d = new Date();
		d.setTime(d.getTime() + exp*1000);
		exp = props.expires = d;
	}
	if (exp && exp.toUTCString) props.expires = exp.toUTCString();

	value = encodeURIComponent(value);
	var updatedCookie = name + "=" + value;

	for(var propName in props){
		updatedCookie += "; " + propName;
		var propValue = props[propName];
		if(propValue !== true) updatedCookie += "=" + propValue;
	}

	document.cookie = updatedCookie;
}

function getCookie(name) {
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(";", offset)
			if (end == -1) {
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	return(setStr);
}

function deleteCookie(name) {
	setCookie(name, null, { expires: -1 });
}


// isFunction alternative
/*
function isFunction(object) {
	return object && getClass.call(object) == '[object Function]';
}

function isFunction(object) {
	if (typeof object != 'function') return false;
	var parent = object.constructor && object.constructor.prototype;
	return parent && hasProperty.call(parent, 'call');
}
*/
function isFunction(object) {
	return !!(object && object.constructor && object.call && object.apply);
}


// page offset

function getPageSize() {
	var xScroll, yScroll;
	if (window.innerHeight && window.scrollMaxY) {	
		xScroll = window.innerWidth + window.scrollMaxX;
		yScroll = window.innerHeight + window.scrollMaxY;
	} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
		xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}
	var windowWidth, windowHeight;
	if (self.innerHeight) {	// all except Explorer
		if(document.documentElement.clientWidth){
			windowWidth = document.documentElement.clientWidth; 
		} else {
			windowWidth = self.innerWidth;
		}
		windowHeight = self.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	} else if (document.body) { // other Explorers
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}	
	// for small pages with total height less then height of the viewport
	if(yScroll < windowHeight){
		pageHeight = windowHeight;
	} else { 
		pageHeight = yScroll;
	}
	// for small pages with total width less then width of the viewport
	if(xScroll < windowWidth){	
		pageWidth = xScroll;		
	} else {
		pageWidth = windowWidth;
	}
	arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight);
	return arrayPageSize;
};

function getPageScroll() {
	var xScroll, yScroll;
	if (self.pageYOffset) {
		yScroll = self.pageYOffset;
		xScroll = self.pageXOffset;
	} else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
		yScroll = document.documentElement.scrollTop;
		xScroll = document.documentElement.scrollLeft;
	} else if (document.body) {// all other Explorers
		yScroll = document.body.scrollTop;
		xScroll = document.body.scrollLeft;	
	}
	arrayPageScroll = new Array(xScroll,yScroll);
	return arrayPageScroll;
};


// check chars in field

function checkChars(e, goods, id) {
	  var key, keychar;
	  key = getKey(e);
	  if (key == null)
		   return true;

	  // get character
	  keychar = String.fromCharCode(key);
	  keychar = keychar.toLowerCase();
	  goods = goods.toLowerCase();

	  // check goodkeys
	  if (goods.indexOf(keychar) != -1) {
		   return true;
	  }

	  // control keys
	  if ( key==null || key==0 || key==8 || key==9 || key==13 || key==27 ) {
		   return true;
	  }

	  // else return false
	  return false;
}
function getKey(e) {
	if (window.event) return window.event.keyCode;
	else if (e) return e.which;
	else return null;
}


// format number

function unformat(value) {

	// Fails silently (need decent errors):
	value = value || 0;

	// Return the value as-is if it's already a number:
	if (typeof value === "number") return value;

	// Default decimal point comes from settings, but could be set to eg. "," in opts:
	var decimal = '.';

	 // Build regex to strip out everything except digits, decimal point and minus sign:
	var regex = new RegExp("[^0-9-" + decimal + "]", ["g"]),
		unformatted = parseFloat(
			("" + value)
			.replace(/\((.*)\)/, "-$1") // replace bracketed values with negatives
			.replace(regex, '')         // strip out any cruft
			.replace(decimal, '.')      // make sure decimal point is standard
		);

	// This will fail silently which may cause trouble, let's wait and see:
	return !isNaN(unformatted) ? unformatted : 0;
};

function toFixed(value, precision) {
	var power = Math.pow(10, precision);

	return (Math.round(unformat(value) * power) / power).toFixed(precision);
};

function formatNumber(number, precision) {

	// Clean up number:
	number = unformat(number);
	if (number == 0) return 0;

	// Build options object from second param (if object) or all params, extending defaults:
	var opts = {
			'precision' : (precision === undefined || precision === false ? false : precision),
			thousand : ' ',
			decimal : '.'
		},
		negative = number < 0 ? "-" : "",
		base = parseInt(toFixed(Math.abs(number || 0), opts.precision), 10) + "",
		mod = base.length > 3 ? base.length % 3 : 0;

	// Format the number:
	if (opts.precision === false) {
		var decPart = (number+".").split('.')[1];
		return negative + (mod ? base.substr(0, mod) + opts.thousand : "") + base.substr(mod).replace(/(\d{3})(?=\d)/g, "$1" + opts.thousand) + (parseInt(decPart) > 0 ? opts.decimal + decPart : '');
	} else {
		if (opts.precision > 0) {
			return negative + (mod ? base.substr(0, mod) + opts.thousand : "") + base.substr(mod).replace(/(\d{3})(?=\d)/g, "$1" + opts.thousand) + opts.decimal + toFixed(Math.abs(number), opts.precision).split('.')[1];
		} else {
			return negative + (mod ? base.substr(0, mod) + opts.thousand : "") + base.substr(mod).replace(/(\d{3})(?=\d)/g, "$1" + opts.thousand);
		}
	}
};




//
// (depend :global)
//

function callHandler(handlerFunctionName, handlerParameter) {
	if (handlerFunctionName && window[handlerFunctionName] && isFunction(window[handlerFunctionName])) {
		window[handlerFunctionName](handlerParameter);
		return true;
	}
	return false;
}





