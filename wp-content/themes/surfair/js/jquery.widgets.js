
$.fn.popup = function(settings){
	if ( this[0].popupInited === undefined) {
		//
		// properties
		//

		var self = $.extend(this[0], {

			// init flag

			popupInited : true,


			// settings

			settings : $.extend({
				'title' : null,
				'content' : null,

				'inited' : null,
				'beforeShow' :  null,
				'afterShow' :  null,
				'beforeHide' : null,
				'afterHide' :  null
			}, settings),


			// ui

			contentObj : $('<div>'),
		});



		// init

		self.contentObj.append(self.settings.content).dialog({
			autoOpen : false,
			modal : true,
			resizable : false,
			closeOnEscape : false,
			minHeight : 0,

			title : self.settings.title,
			width : ( parseInt($(settings.content).css('width'))||400 ),

			close : function(event, ui) {
				$(self.settings.content).find('input, select, textarea, button').blur();

				if ( self.settings.afterHide && $.isFunction(self.settings.afterHide) ) {
					self.settings.afterHide(self.contentObj);
				}
			},
			
			open : function(event, ui) {
				if ( self.settings.afterShow && $.isFunction(self.settings.afterShow) ) {
					self.settings.afterShow(self.contentObj);
				}
			}
		});

		if (!self.settings.title)
			self.contentObj.dialog('widget').find('.ui-dialog-title').hide();


		$(self.settings.content).show();

		$(self).click(function(){
			self.contentObj.dialog('open');
			return false;
		});


		// inited callback

		if ( self.settings.inited && $.isFunction(self.settings.inited) ) {
			self.settings.inited(self.contentObj);
		}




	} else {
		var self = this[0];
	}


	//
	// functions
	//

	function _show() {
		if ( self.settings.beforeShow && $.isFunction(self.settings.beforeShow) ) {
			if (self.settings.beforeShow(self.contentObj)) {
				self.contentObj.dialog('open');
			}
		} else {
			self.contentObj.dialog('open');
		}
	}

	function _hide() {
		if ( self.settings.beforeHide && $.isFunction(self.settings.beforeHide) ) {
			if (self.settings.beforeHide(self.contentObj)) {
				self.contentObj.dialog('close');
			}
		} else {
			self.contentObj.dialog('close');
		}
	}


	//
	// actions
	//

	if (typeof(settings) == 'string') {
		switch (settings) {
			case 'show':
				_show();
				break;

			case 'hide':
				_hide();
				break;
		}
	}


	return self;
};


$.fn.visibleTrigger = function(content, settings){

	if ($(this).size() > 0) {
		var titleObj = $(this);

		if ( this[0].visibleTriggerInited === undefined) {
			//
			// init
			//

			var self = $.extend(this[0], {

				visibleTriggerInited : true,
				content : $(content),

				// settings

				settings : $.extend({
					'defaultStatus' : false,
					'showCallback' : null,
					'afterHide' : null
				}, settings)
			});

			if ( !titleObj.size() || !self.content.size() ) return false;



			titleObj.unbind('click').click(function(){
				if (self.content.is(':visible')) _hide();
				else _show();

				return false;
			}).css({
				'cursor' : 'pointer'
			});

			if ( self.settings.defaultStatus ) {
				_show();
			} else {
				_hide();
			}


		} else {
			var self = this[0];
		}


		//
		// methods
		//

		if (typeof(content) == 'string') {
			switch (content) {
				case 'show':
					_show();
					break;

				case 'hide':
					_hide();
					break;

				case 'refresh':
					if ( self.settings.defaultStatus ) {
						_show();
					} else {
						_hide();
					}
					break;
			}
		}
	}



	//
	// functions
	//

	function _show() {
		titleObj.removeClass('status-hide').addClass('status-show').blur();
		self.content.removeClass('status-hide').addClass('status-show').show();

		if ( self.settings.showCallback && $.isFunction(self.settings.showCallback) ) self.settings.showCallback(titleObj, self.content);
	}
	function _hide() {
		titleObj.removeClass('status-show').addClass('status-hide').blur();
		self.content.removeClass('status-show').addClass('status-hide').hide();

		if ( self.settings.afterHide && $.isFunction(self.settings.afterHide) ) self.settings.afterHide(titleObj, self.content);
	}

	return $(this);
};

