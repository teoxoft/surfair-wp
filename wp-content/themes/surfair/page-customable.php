<?php
/**
 * Template Name: Customable Template
 * Description: A Page Template without header and with free layout
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

	<!-- template page-clean.php -->

	<div class="overflow">
		<?php dynamic_sidebar('page-before'); ?>
	</div>

	<?php while ( have_posts() ) : the_post(); ?>
		<div class="overflow">
			<?php
				require_once get_template_directory().'/inc/page-layout.php';


				// top custom sidebar

				if (get_field('LAYOUT_banner')) 	echo get_page_banner();

				if (get_field('LAYOUT_benefits')) 	echo get_benefit_buckets();



				// content
				if (get_field('CONTENT_request'))	echo get_form_request();
				echo '<div class="page-wrapper wrapper clearfix">';
					the_content();
				echo '</div>';

				
				// bottom custom sidebar

				if (get_field('LAYOUT_request'))	echo get_form_request();
				if (get_field('LAYOUT_subscribe'))	echo get_form_subscribe();
				if (get_field('LAYOUT_waitlist'))	echo '<div class="wrapper">'. get_form_waitlist() .'</div>';

			?>
		</div>
	<?php endwhile;?>

	<div class="overflow">
		<?php dynamic_sidebar('page-after'); ?>
	</div>

<?php get_footer(); ?>