<?php

/*
 * Post Layouts
 * - must be in the WP post loop
 */

function get_post_layout($thumbnail, $date, $excerpt) {
//	global $post;

	$type = get_field('post_type');

	switch ($type) {
		case 'press':
			$link_title = get_field('PRESS_link_title');
			$link_url = get_field('PRESS_link_url');


			// html

			$output = '<div class="infoblock clearfix">';
				if ($thumbnail == 'true') {
					if ($link_url) $output .= '<a target="_blank" class="link" href="http://'.$link_url.'">';
						$output .= '<div class="infoblock-image">';
							if ( has_post_thumbnail($post->ID) ) {
								$output .= '<img src="'.wp_get_attachment_url( get_post_thumbnail_id($post->ID) ).'" />';
							} else if ($link_title) {
								$output .= '<span class="no-image">'.$link_title.'</span>';
							}
						$output .= '</div>';
					if ($link_url) $output .= '</a>';
				}

				$output .= '<div class="overflow">';
					if ($link_url) $output .= '<a target="_blank" class="link" href="http://'.$link_url.'">';
						$output .= '<div class="infoblock-title">'.get_the_title().'</div>';
					if ($link_url) $output .= '</a>';

					if ($link_url) $output .= '<a target="_blank" class="link" href="http://'.$link_url.'">';
						if ($link_title) $output .= '<div class="link-title">'.$link_title.'</div>';
						// if ($date == 'true') {
						//	$output .= '<div class="infoblock-date">'.get_the_date().'</div>';
						// }
					if ($link_url) $output .= '</a>';

					$output .= '<div class="infoblock-body content">'.get_the_content().'</div>';
				$output .= '</div>';
			$output .= '</div>'; // end .infoblock
		break;


		case 'banner':
			$background_color = get_field('BANNER_background_color');
			$background_image = get_field('BANNER_background_image');

			if ( $background_image ) {
				$background_image_size = getimagesize($background_image);
				$background_repeat = get_field('BANNER_background_repeat');
				$background_position = get_field('BANNER_background_position');

				$style = 'background:'.($background_color ? $background_color : '').' url('.$background_image.') '.($background_repeat ? $background_repeat : 'no-repeat').' '.($background_position ? $background_position : '50% 50%').'; max-height:'.$background_image_size[1].'px;';
			} else if ( $background_color ) {
				$style = 'background-color:'.$background_color.';';
			}


			// html

			$output = '<div class="infoblock post-banner" style="'.$style.'">';
				$output .= '<div class="infoblock-body content">'.get_the_content().'</div>';
			$output .= '</div>'; // end .infoblock
		break;


		case 'video':
			$video_thumb = get_field('VIDEO_thumb');
			$video_source = get_field('VIDEO_source');


			// html

			$output .= '<div class="infoblock">';
				$output .= '<div class="infoblock-image">';
					$output .= '<a href="'.$video_source.'">';
						$output .= '<img src="'.$video_thumb.'" />';
					$output .= '</a>';
				$output .= '</div>';
				$output .= '<div class="infoblock-title">'.get_the_title().'</div>';
			$output .= '</div>'; // end .infoblock
		break;


		case 'map':
			$coords = get_field('MAP_coords');
			$token = get_field('MAP_token');
			$address = get_field('MAP_address');
			$code = get_field('MAP_code');
			$googleMap = get_field('MAP_googlemap');
			$brief = get_field('MAP_brief');
			$content = get_the_content();


			// html

			$output = '<div class="infoblock" id="'.$token.'" data-coords="'.$coords.'">';
				if ($code) $output .= '<div class="map-3lcode">'.$code.'</div>';
				$output .= '<div class="infoblock-title">'.get_the_title().'</div>';
				if ($address) $output .= '<div class="map-address">'.$address.'</div>';
				$output .= '<div class="map-google"><a target="blank" href="https://www.google.com/maps/dir//'.str_replace('+'," ",$googleMap['address']).'/@'.$googleMap['lat'].','.$googleMap['lng'].'/" class="btn">Map&nbsp;&nbsp;<i class="fa fa-map-marker"></i></a></div>';
				$output .= '<div class="map-brief content">';
				$output .= ($content) ? $brief.'<a href="#more" class="more">More</a>' : $brief;
				$output .= '</div>';

				//if ($content) {
					$output .= '<div class="infoblock-body content">'.get_the_content().'</div>';
				//}


				$las = (get_the_title() == "Las Vegas");
                if(!$las){
                	$output .= '<div class="map-buttons">';
                	$output .= '<a href="/waitlist.html?city='.$token.'" class="btn btn-primary">Join in '.get_the_title().'</a>';
                	$output .= '</div>';
                }
			$output .= '</div>'; // end .infoblock
		break;


		case 'basic':
		default:
			$output = '<div class="infoblock clearfix">';
				if ($thumbnail == 'true') {
					if ( has_post_thumbnail($post->ID) ) {
						$output .= '<div class="infoblock-image"><img src="'.wp_get_attachment_url( get_post_thumbnail_id($post->ID) ).'" /></div>';
					}
				}

				$output .= '<div class="overflow">';
					$output .= '<div class="infoblock-title">'.get_the_title().'</div>';
					if ($date == 'true') {
						$output .= '<div class="infoblock-date">'.get_the_date().'</div>';
					}
					if ($excerpt == 'true') {
						$output .= '<div class="infoblock-excerpt">'.get_the_excerpt().'</div>';
					}
					$output .= '<div class="infoblock-body content">'.get_the_content().'</div>';
				$output .= '</div>';
			$output .= '</div>'; // end .infoblock
		break;
	}


	return $output;
}

?>