<?php

require_once get_template_directory().'/inc/post-layout.php';
require_once get_template_directory().'/inc/page-layout.php';


function surfair_cat($atts) {
	extract(shortcode_atts(array(
		'id'		=> '',
		'total'		=> '-1',

		'class'		=> '',
		'heading'	=> 'false',
		'thumbnail' => 'false',
		'date'      => 'false',
		'excerpt'   => 'false'
	), $atts));


	$output = '<div class="'.$class.'">';
	$output .= '<div class="infoblock-container">';

	//
	// category
	//

	if ($heading == 'true') {
		$category_title = get_the_category_by_ID($id);
		$output .= '<h2 class="infoblock-heading">'.$category_title.'</h2>';
	}


	//
	// posts
	//

	global $post;
	$tmp_post = $post;

	$posts = get_posts(array(
		category => $id,
		numberposts => $total,
		orderby => 'post_date',
		order => 'ASC'
	));

	// html

	$output .= '<div class="infoblock-list">';
	foreach($posts as $post) {
		setup_postdata($post);

		$output .= get_post_layout($thumbnail, $date, $excerpt);
		$output .= ' '; // important space after item section
	};
	$output .= '</div>';

	$output .= '</div>'; // end .infoblock-container
	$output .= '</div>'; // end root


	// reset

	$post = $tmp_post;
	wp_reset_query();


	return $output;
}
add_shortcode('cat', 'surfair_cat');




function surfair_post($atts) {
	extract(shortcode_atts(array(
		'id'		=> '',

		'class'		=> '',
		'thumbnail' => 'false',
		'date'      => 'false',
		'excerpt'   => 'false'
	), $atts));

	//
	// post
	//

	global $post;
	$tmp_post = $post;

	$post = get_post($id);
	setup_postdata($post);

	// html

	$output = '<div class="'.$class.'">';
	$output .= get_post_layout($thumbnail, $date, $excerpt);
	$output .= '</div>'; // end root

	// reset

	$post = $tmp_post;
	wp_reset_query();


	return $output;
}
add_shortcode('post', 'surfair_post');



function surfair_form($atts) {
	extract(shortcode_atts(array(
		'id'		=> '',
		'class'		=> '',

		'h3'		=> '',
		'p'			=> ''
	), $atts));

	// html

	$output = '<div class="'.$class.'">';
	switch ($id) {
		case 'request':
			$output .= get_form_request($h3);
		break;

		case 'subscribe':
			$output .= get_form_subscribe($h3, $p);
		break;

		case 'waitlist':
			$output .= get_form_waitlist();
		break;

		case 'lead':
        			$output .= get_form_lead();
        		break;
        case 'benefits':
				$output .= get_benefit_buckets();
			break;
	}
	$output .= '</div>'; // end root

	return $output;
}
add_shortcode('form', 'surfair_form');


?>
