<?php

function get_page_banner() {
	$background_color = get_field('background_color');
	$background_image = get_field('background_image');
	$content = get_field('content');

	if ( $background_image ) {
		$background_image_size = getimagesize($background_image);
		$background_repeat = get_field('background_repeat');
		$background_position = get_field('background_position');

		$style = 'background:'.($background_color ? $background_color : '').' url('.$background_image.') '.($background_repeat ? $background_repeat : 'no-repeat').' '.($background_position ? $background_position : '50% 50%').'; min-height:'.$background_image_size[1].'px;';

	} else if ( $background_color ) {
		$style = 'background-color:'.$background_color.';';
	}


	// html

	$output = '<div class="page-banner" style="'.$style.'">'.($content ? $content : '&nbsp;').'</div>';

	return $output;
}


function get_benefit_buckets() {

	$output = '
<div class="colpane content buckets-content">
<div class="inner-benefits">
	<div class="col13 benefit-bucket">
		<div class="colpane">
			<div class="col15">
				<span class="icon-svg2 icon-surf-xl"></span>
			</div>
			<div class="col45 bucket-content">
				<h3 class="bucket-title">Always Fast</h3>
				<p>Book a flight in 30 seconds, check in up to 15 minutes before takeoff.
				</p>
			</div>
		</div>
	</div>
	<div class="col13 benefit-bucket">
		<div class="colpane">
			<div class="col15">
				<span class="icon-svg3 icon-surf-xl"></span>
			</div>
			<div class="col45 bucket-content">
				<h3 class="bucket-title">Always Comfortable</h3>
				<p>In a BMW-designed eight-person cabin, every seat is First Class.
				</p>
			</div>
		</div>
	</div>
	<div class="col13 benefit-bucket">
		<div class="colpane">
			<div class="col15">
				<span class="icon-svg4 icon-surf-xl"></span>
			</div>
			<div class="col45 bucket-content">
				<h3 class="bucket-title">Always Welcome</h3>
				<p>With no lines and no wait, nothing stands between you and the joy of flying.
				</p>
			</div>
		</div>
	</div>
</div>
</div>
	';

	return $output;
}




//
// Forms
//

function get_form_request($h3 = '') {
	if (!$h3) $h3 = '<span class="lets-talk">Let\'s talk.</span><br /> Membership starts with a simple introduction.';


	$output = '
<div class="form-request">
	<div class="wrapper">
		<h3>'.$h3.'</h3>

		<form method="POST" action="https://go.pardot.com/l/44522/2014-08-08/bj5k">
			<input name="lead_source" value="More Info" type="hidden">
			<input name="oid" value="00Di0000000bg1R" type="hidden">
			<input name="retURL" value="'.get_bloginfo('siteurl').'/thank-you-request.html" type="hidden">
			<input name="00Ni0000007biZp" id="00Ni0000007biZp" value="" type="hidden">
			<input name="00Ni000000FR7yk" id="00Ni000000FR7yk" value="more-info" type="hidden">
			<input name="00Ni000000FR7zJ" id="00Ni000000FR7zJ" value="" type="hidden">
			<input id="referrer" maxlength="255" name="referredFrom" size="20" type="hidden"
                                               value=""/>

			<div class="form-group clearfix">
				<input name="last_name" placeholder="First Name" type="text" class="id-lastname">
				<input name="first_name" placeholder="Last Name" type="text" class="id-firstname">
				<input name="email" placeholder="Your Email" type="text" class="id-email">
				<button type="submit" value="Submit" class="btn btn-primary sub-btn">Submit</button>
			</div>
        </form>
        <script>

        	$("#referrer").val(externalReferrer);
        </script>
	</div>
</div>
	';
                    
	return $output;
}

function get_form_lead($h3 = '') {
	if (!$h3) $h3 = 'Membership starts with a simple introduction.';

	$output = '
<div class="form-lead">
	<div class="lead-form-wrap">
		<h3 class="form-head signup-text">'.$h3.'</h3>

		<form method="POST" action="https://go.pardot.com/l/44522/2014-08-08/bj5k">
			<input name="lead_source" value="More Info" type="hidden">
			<input name="oid" value="00Di0000000bg1R" type="hidden">
			<input name="retURL" value="'.get_bloginfo('siteurl').'/thank-you-request.html" type="hidden">
			<input name="00Ni0000007biZp" id="00Ni0000007biZp" value="" type="hidden">
			<input name="00Ni000000FR7yk" id="00Ni000000FR7yk" value="more-info" type="hidden">
			<input name="00Ni000000FR7zJ" id="00Ni000000FR7zJ" value="" type="hidden">
			<input id="referrer" maxlength="255" name="referredFrom" size="20" type="hidden"
                                               value=""/>

			<div class="form-group clearfix">
				<input name="last_name" placeholder="First Name" type="text" class="id-lastname">
				<input name="first_name" placeholder="Last Name" type="text" class="id-firstname">
				<input name="email" placeholder="Your Email" type="text" class="id-email">
				<button type="submit" value="Submit" class="btn btn-primary sub-btn">Submit</button>
			</div>
		</form>
		<script>

        	$("#referrer").val(externalReferrer);

        </script>
	</div>
</div>
	';

	return $output;
}

function get_form_subscribe($h3 = '', $p = '') {
	if (!$h3) $h3 = 'Stay in the loop.';
	if (!$p) $p = 'Receive latest happenings, Surf Air monthly offers, and great reviews.';

	$output = '
<div class="form-subscribe">
	<div class="wrapper">
		<h3>'.$h3.'</h3>
		<p>'.$p.'</p>

		<form method="post" action="/air.do/v/json/interest">
			<input name="action" value="insert" type="hidden">

			<div class="form-group">
				<input name="email" placeholder="Your Email" type="text"><input value="Submit" class="btn btn-primary" type="submit">
			</div>
		</form>
	</div>
</div>
	';
                
	return $output;
}

function get_form_waitlist() {
	$output = '
<script language="javascript">
	function setCo(){
		document.getElementById("company").value = document.getElementById("name").value+" - Personal";
	}
</script>

<div class="form-waitlist" id="waitlist-form">
	<form method="POST" action="http://go.pardot.com/l/44522/2014-07-11/6mfh">
		<input name="lead_source" id="lead_source" value="Waitlist" type="hidden">
		<input name="oid" value="00Di0000000bg1R" type="hidden">
		<input name="first_name" id="first_name" type="hidden">

		<input name="retURL" value="'.get_bloginfo('siteurl').'/thank-you-waitlist.html" type="hidden">
		<input name="00Ni0000007biZp" id="00Ni0000007biZp" value="" type="hidden">
		<input name="00Ni000000FR7yk" id="00Ni000000FR7yk" value="more-info" type="hidden">
		<input name="00Ni000000FR7zJ" id="00Ni000000FR7zJ" value="" type="hidden">
		<input name="00Ni0000007biZp" id="00Ni0000007biZp" value="'.get_bloginfo('siteurl').'/waitlist.html" type="hidden">

		<div class="colpane">

			<div class="col12">
				<div class="form-group">
					<div class="form-label">Full Name*</div>
					<div class="form-stretch">
						<input name="last_name" id="last_name" placeholder="Full Name" type="text" onblur="setCo()">
						<input name="company" id="company" type="hidden">
					</div>
				</div>

				<div class="form-group">
					<div class="form-label">Your Email*</div>
					<div class="form-stretch">
						<input id="email" name="email" placeholder="Enter Email" required="" type="text">
					</div>
				</div>

				<div class="form-group">
					<div class="form-label">Your Phone*</div>
					<div class="form-stretch">
						<input placeholder="XXX-XXX-XXXX" id="phone" name="phone" required="" type="text">
					</div>
				</div>

				<div class="form-group">
					<div class="form-label">City*</div>
					<div class="form-stretch">
						<input name="city" id="city" placeholder="Enter City" required="" type="text">
					</div>
				</div>

				<div class="form-group">
					<div class="form-label">State</div>
					<div class="form-stretch">
						<select name="state">
							<option value="AL">Alabama</option>
							<option value="AK">Alaska</option>
							<option value="AZ">Arizona</option>
							<option value="AR">Arkansas</option>
							<option value="CA" selected="">California</option>
							<option value="CO">Colorado</option>
							<option value="CT">Connecticut</option>
							<option value="DE">Delaware</option>
							<option value="DC">District Of Columbia</option>
							<option value="FL">Florida</option>
							<option value="GA">Georgia</option>
							<option value="HI">Hawaii</option>
							<option value="ID">Idaho</option>
							<option value="IL">Illinois</option>
							<option value="IN">Indiana</option>
							<option value="IA">Iowa</option>
							<option value="KS">Kansas</option>
							<option value="KY">Kentucky</option>
							<option value="LA">Louisiana</option>
							<option value="ME">Maine</option>
							<option value="MD">Maryland</option>
							<option value="MA">Massachusetts</option>
							<option value="MI">Michigan</option>
							<option value="MN">Minnesota</option>
							<option value="MS">Mississippi</option>
							<option value="MO">Missouri</option>
							<option value="MT">Montana</option>
							<option value="NE">Nebraska</option>
							<option value="NV">Nevada</option>
							<option value="NH">New Hampshire</option>
							<option value="NJ">New Jersey</option>
							<option value="NM">New Mexico</option>
							<option value="NY">New York</option>
							<option value="NC">North Carolina</option>
							<option value="ND">North Dakota</option>
							<option value="OH">Ohio</option>
							<option value="OK">Oklahoma</option>
							<option value="OR">Oregon</option>
							<option value="PA">Pennsylvania</option>
							<option value="RI">Rhode Island</option>
							<option value="SC">South Carolina</option>
							<option value="SD">South Dakota</option>
							<option value="TN">Tennessee</option>
							<option value="TX">Texas</option>
							<option value="UT">Utah</option>
							<option value="VT">Vermont</option>
							<option value="VA">Virginia</option>
							<option value="WA">Washington</option>
							<option value="WV">West Virginia</option>
							<option value="WI">Wisconsin</option>
							<option value="WY">Wyoming</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="form-label">Company</div>
					<div class="form-stretch">
						<input name="00Ni0000007Xhwy" id="00Ni0000007Xhwy" placeholder="Enter Company Name" type="text">
					</div>
				</div>

				<div class="form-group">
                					<div class="form-label">Title</div>
                					<div class="form-stretch">
                						<select name="title">
                							<option value="">Select a title...</option>
                							<option value="CEO">CEO</option>
                							<option value="Founder">Founder</option>
                							<option value="Partner">Partner</option>
                							<option value="C-Level">C-Level</option>
                							<option value="VP">VP</option>
                							<option value="Director">Director</option>
                							<option value="Analyst">Analyst</option>
                							<option value="Other">Other</option>
                						</select>
                					</div>
                				</div>

			</div>



			<div class="col12">



				<div class="form-group">
					<div class="form-label">Home Airport*</div>
					<div class="form-stretch">
						<select name="00Ni0000006cDH6" id="00Ni0000006cDH6">
							<option value="">Select an Airport...</option>
							<option value="SF Bay / San Carlos (SQL)">SF Bay / San Carlos (SQL)</option>
							<option value="Los Angeles / Hawthorne (HHR)">Los Angeles / Hawthorne (HHR)</option>
							<option value="Los Angeles / Burbank (BUR)">Los Angeles / Burbank (BUR)</option>
							<option value="Santa Barbara (SBA)">Santa Barbara (SBA)</option>
							<option value="Truckee / Tahoe (TRK)">Truckee / Tahoe (TRK)</option>
							<option value="SF Bay / Oakland (OAK)">SF Bay / Oakland (OAK)</option>
							<option value="Carlsbad (CRQ)">Carlsbad (CRQ)</option>
							<option value="Las Vegas, NV (LAS)">Las Vegas, NV (LAS) -- charter --</option>
							<option value="San Diego (SAN)">San Diego (SAN) -- planned --</option>
							<option value="Orange County / John Wayne (SNA)">Orange County / John Wayne (SNA) -- planned --</option>
							<option value="Sacramento (SAC)">Sacramento (SAC) -- planned --</option>
							<option value="Monterey (MRY)">Monterey (MRY) -- planned --</option>
							<option value="Palm Springs (PSP)">Palm Springs (PSP) -- planned --</option>
							<option value="Napa / Sonoma (STS)">Napa / Sonoma (STS) -- planned --</option>
							<option value="Mammoth (MMH)">Mammoth Lakes (MMH) -- planned --</option>
							<option value="Scottsdale, AZ (SDL)">Scottsdale, AZ (SDL) -- planned --</option>
							<option value="Outside California">Outside California</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="form-label">Main Destination Airport*</div>
					<div class="form-stretch">
						<select name="00Ni0000008krbS" id="00Ni0000008krbS">
							<option value="">Select an Airport...</option>
							<option value="SF Bay / San Carlos (SQL)">SF Bay / San Carlos (SQL)</option>
							<option value="Los Angeles / Hawthorne (HHR)">Los Angeles / Hawthorne (HHR)</option>
							<option value="Los Angeles / Burbank (BUR)">Los Angeles / Burbank (BUR)</option>
							<option value="Santa Barbara (SBA)">Santa Barbara (SBA)</option>
							<option value="Truckee / Tahoe (TRK)">Truckee / Tahoe (TRK)</option>
							<option value="SF Bay / Oakland (OAK)">SF Bay / Oakland (OAK)</option>
							<option value="Carlsbad (CRQ)">Carlsbad (CRQ)</option>
							<option value="Las Vegas, NV (LAS)">Las Vegas, NV (LAS) -- charter --</option>
							<option value="San Diego (SAN)">San Diego (SAN) -- planned --</option>
							<option value="Orange County / John Wayne (SNA)">Orange County / John Wayne (SNA) -- planned --</option>
							<option value="Sacramento (SAC)">Sacramento (SAC) -- planned --</option>
							<option value="Monterey (MRY)">Monterey (MRY) -- planned --</option>
							<option value="Palm Springs (PSP)">Palm Springs (PSP) -- planned --</option>
							<option value="Napa / Sonoma (STS)">Napa / Sonoma (STS) -- planned --</option>
							<option value="Mammoth (MMH)">Mammoth Lakes (MMH) -- planned --</option>
							<option value="Scottsdale, AZ (SDL)">Scottsdale, AZ (SDL) -- planned --</option>
							<option value="Outside California">Outside California</option>
							<option value="0">Other</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="form-label">Primary Reason for Travel</div>
					<div class="form-stretch">
						<select id="00Ni000000ES0br" name="00Ni000000ES0br">
							<option value="Business" selected="">Business</option>
							<option value="Leisure">Leisure</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="form-label">How often to you expect to travel?</div>
					<div class="form-stretch">
						<select id="00Ni0000006cDbF" name="00Ni0000006cDbF">
							<option value="Daily">Daily</option>
							<option value="Weekly" selected="">Weekly</option>
							<option value="Bi-Monthly">Twice per month</option>
							<option value="Monthly">Monthly</option>
						</select>
					</div>
				</div>
							
				<div class="form-group">
					<div class="form-label">&nbsp;</div>
					<div class="clearfix">
						<div class="form-text left">*Required fields</div>
						<button type="submit" value="Submit" class="btn btn-primary right btn-primary-form">SUBMIT APPLICATION</button>
					</div>
				</div>

			</div>
		</div>

	</form>

<script>

	$("#referrer").val(externalReferrer);

</script>

</div>
	';
                    
	return $output;
}


?>