<?php
/**
 * Template Name: Index Template
 * Description: A Page Template that usig for index page
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

	<!-- template page-index.php -->

	<div class="clearfix">

		<div class="wrapper clearfix">
			<?php dynamic_sidebar('index'); ?>
		</div>

		<div class="wrapper">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile;?>
		</div>

	</div>

<?php get_footer(); ?>